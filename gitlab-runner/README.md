# Kubernetes-gitlab-runner

## Pre-requisite
- Running Kubernetes cluster or Openshift cluster
- `Helm CLI` configured
- `OC CLI` configured
- `Kubectl CLI` configured

## Context
We are going to use the `Helm` to configure the `gitlab-runner` on Openshift cluster running locally.

## Steps
### Update `values.yaml` file
We need to update the `values.yaml` file with `runnerRegistrationToken` and `gitlabUrl`. That is the basic configuration we need to update.
```yaml
gitlabUrl: https://gitlab.com/

runnerRegistrationToken: "GR1348941syS-pfGVPAs"
```

### Update the `SecurityContext`
Update it like as below
```yaml
securityContext:
  seccompProfile:
    type: "RuntimeDefault"
  allowPrivilegeEscalation: false
  readOnlyRootFilesystem: false
  runAsNonRoot: true
  privileged: false
  capabilities:
    drop: ["ALL"]
```

### Update `podSecurityContext`
```yaml
podSecurityContext:
  runAsUser: 1000650002
  # runAsGroup: 2324
  fsGroup: 1000650000
```

### Add/Update `volumes`
```yaml
volumes:
  - emptyDir: {}
    name: gitlab-runner

volumeMounts:
  - name: gitlab-runner
    mountPath: /.gitlab-runner
```

### Update `tags` and `name` of the runner
```yaml
tags: "test,ocp"

## Specify the name for the runner.
##
name: "test-ocp"
```

### Execute below command via `helm` cli to install the runner
`$ helm install --namespace argocd gitlab-runner -f ./values.yaml gitlab/gitlab-runner`

- `argocd` is the namespace where we are going to install the runner
- `gitlab-runner` is the name of the runner pod

### Un-install the runner
`$ helm uninstall gitlab-runner -n argocd`

### Update the `privileged` scc
In case if you're facing some permission denied error. Then edit the `privileged` SCC and the below line under users section

`$ oc edit scc privileged`  
then  
`- system:serviceaccount:$GITLAB_RUNNER_NAMESPACE:$GITLAB_RUNNER_SA`

### References
- https://docs.gitlab.com/runner/install/kubernetes.html#configuring-gitlab-runner-using-the-helm-chart
