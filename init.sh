#!/bin/bash

echo "Installing Kubectl"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

echo "Installing ArgoCD"
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd

echo "Installing OC CLI"
# https://mirror.openshift.com/pub/openshift-v4/aarch64/clients/ocp/
curl -LO https://mirror.openshift.com/pub/openshift-v4/aarch64/clients/ocp/4.10.0-rc.0/openshift-client-linux-amd64-4.10.0-rc.0.tar.gz
tar -zxvf openshift-client-linux-amd64-4.10.0-rc.0.tar.gz
mv oc /usr/local/bin/oc
chmod +x /usr/local/bin/oc