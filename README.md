# custom-image

## Overview
This is a custom image built on `centos` image.  
`FROM centos:latest`

We have built this image to install/configure some CLIs.

This image are going to be use as a base image for gitlab-runner and perform the action.

CLIs are
- `kubectl`
- `OC`
- `helm`
- `argocd`

Once the image is ready, we can use this in one of the gitlab-ci stage.

## Build Image via `kaniko`  
We will be creating this image by running a pipeline via gitlab-runner, running on kubernetes.
Also, we are using kaniko build to build this image.

## Config Runner  
We will be configuring the runner via `helm`. Installation example and `values.yaml` can be found under `gitlab-runner` directory and steps to config the same.