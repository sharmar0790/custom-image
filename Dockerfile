FROM centos:latest

COPY ./init.sh  /init.sh

RUN cd / && chmod +x ./init.sh && ./init.sh && yum clean all

CMD /bin/bash
